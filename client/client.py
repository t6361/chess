from typing import Any, Dict, Literal
from itertools import product
from custom_errors import *
from check_functions import *
from metods import *
from game import *
import requests


def cout_boards(boards) -> None:
    for key in boards:
        print(f"    ● {key}")


def register_and_create_session(url: str) -> tuple[str, str, str, str]:
    name = input("Введите имя: ")
    name = check_registration(registration(url, name))
    token = input("Введите токен: ")
    password = input("Придумайте надежный пароль")
    board_id = check_create_session(create_session(url, token, password))
    return name, token, password, board_id


def login_and_make_choise(url) -> tuple[str, str, str, str]:
    token = input("Введите токен: ")
    name = check_login(login(url, token))
    all_boards = check_get_all_sessions(get_all_sessions(url, token))
    print(f"Здравствуйте, {name}!\nУ Вас {len(all_boards)} комнат")
    if len(all_boards) == 0:
        print("Давайте, создадим комнату")
        password = input("Придумайте надежный пароль: ")
        board_id = check_create_session(create_session(url, token, password))
    else:
        c = input("Выбрать существующую комнату или создать новую? (1/2) ")
        if c == 1:
            cout_boards(all_boards)
            room = int(input("\nВыберите идентификатор комнаты: "))
            board_id = check_get_session(get_session(url, room))
        if c == 2:
            password = input("Придумайте надежный пароль: ")
            board_id = check_create_session(create_session(url, token, password))
            print(f"Идентификатор комнаты: {board_id}")
    return name, token, password, board_id


def main(url: str) -> None:
    f = True
    ans = input("У Вас есть токен? (да/нет) ").lower()
    if ans == "нет":
        name, token, password, board_id = register_and_create_session(url)
    while f:
        if ans == "да":
            name, token, password, board_id = login_and_make_choise(url)
        board = check_get_game_field(get_game_field(url, board_id, token, password))
        while answer(board) == "Friendship" and " " in board:
            cout_board(board)
            pos, shape= reading(board)
            check_make_move(make_move(url, board_id, token, password, pos, shape))
            board = check_get_game_field(get_game_field(url, board_id, token, password))
        cout_board(board)
        answer(board)
        if input("Again? (y) ") != "y":
            f = False


if __name__ == "__main__":
    main(url="http://localhost:5000")
