class UnknownTokenError(Exception):
    def __str__(self) -> str:
        return "Ошибка из-за неизвестного токена"


class ValidationError(Exception):
    def __str__(self) -> str:
        return "Oшибка проверки"


class RegistrationError1(Exception):
    def __str__(self) -> str:
        return "Имя пользователя слишком короткое"


class RegistrationError2(Exception):
    def __str__(self) -> str:
        return "Пользователь с таким именем уже существует"


class SessionError(Exception):
    def __str__(self) -> str:
        return "Ошибка при создании сессии"


class RoomNumberError(Exception):
    def __str__(self) -> str:
        return "Несуществующий номер комнаты"


class FourZeroFourError(Exception):
    def __str__(self) -> str:
        return "Ошибка 404"


class Session_ID_Error(Exception):
    def __str__(self) -> str:
        return "Неправильный идентификатор сессии."


class InputError(Exception):
    def __str__(self) -> str:
        return "It's not int [1,9] an empty position"
