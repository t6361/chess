from custom_errors import *


def check_login(ans):
    try:
        name = ans["username"]
        return name
    except:
        pass
    try:
        if ans["message"] == "Неизвестный токен":
            raise UnknownTokenError()
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_registration(ans):
    try:
        message = ans["username"]
        return message
    except:
        pass
    try:
        if ans["message"] == "Имя пользователя слишком короткое.":
            raise RegistrationError1()
    except:
        pass
    try:
        if ans["message"] == "Пользователь с таким именем уже существует":
            raise RegistrationError2()
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_create_session(ans):
    try:
        id = ans["id"]
        return id
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_get_session(ans):
    try:
        if ans is str:
            return ans
    except:
        pass
    try:
        if ans["message"] == "Неправильный идентификатор сессии.":
            raise Session_ID_Error()
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_get_all_sessions(ans):
    try:
        if id == ans["id"]:
            return id
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_get_game_field(ans):
    try:
        if ans is list:
            return ans
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()


def check_make_move(ans):
    try:
        if ans is list:
            return ans
    except:
        pass
    try:
        if ans["detail"]:
            raise ValidationError()
    except:
        raise FourZeroFourError()
