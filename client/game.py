from custom_errors import InputError
from itertools import product


def cout_board(board) -> None:
    print("\n")
    [print(f"    {i+1}", end="") for i in range(8)]
    print(f"\n  {'●━━━━'*8}●")
    for i in range(8):
        print(i+1, end=" ")
        [print(f"┃ {board[i][j]} ", end="") for j in range(8)]
        print(f"┃ {i+1}\n  {'●━━━━'*8}●")
    [print(f"    {i+1}", end="") for i in range(8)]
    print()


def reading(board) -> tuple[int, str] | None:
    step = board.count("x") + board.count("o")
    while 1:
        try:
            pos = int(input(('2nd: ' if (step%2) else '1st: '))) - 1
            if board[pos] == ' ' and pos >= 0:
                return pos, 'o' if (step%2) else 'x'
        except: 
            InputError()


def answer(board) -> str:
    for i, v in product(range(3), [["x"]*3, ["o"]*3]):
        if (board[i::3] == v or board[i*3:i*3+3] == v or
            (i and (board[::4] == v or board[2:7:2] == v))):
            return "1st" if (v[0] == 'x') else "2nd"
    return "Friendship"
